/* 
***********************************
The Revora Network Bar - Bar Setup
***********************************

File: bar_content.js
Author: 
  Original: Blodo [Yuri_S] 
  Current version: Dark Lord of the Sith (DLotS)
Description: This file controls the setup and contents of the network bar (i.e. the links and the structure)



 -- Usage --
Sections:
  rnb_declare('section', new Array('_id', '_name', '_class', '_dclass', '_img'));
  Arguments:
   _id (a unique(!) id of the element)
   _name (title on the section)
   _class (css class of the section button)
   _dclass (css class of the section dropdown)
   _img (if the element is supposed to be an image, this is the path to the image)
	must be an array where the first element is the path to the image
	and the second either 'replace' or 'precede' (so the image will replace or precede the text of the _name parameter)
  
   
Categories:
  rnb_declare('category', new Array('_parent', '_id', '_name', '_addimgsrc', '_img', '_class', '_dclass', '_url'));
  Arguments:
   _parent (_id of the parent tag)
   _id (a unique(!) id of the element)
   _name (title on the button)
   _addimgsrc (path to a small additional image, if one should be added [yes, this is for the small forum/new/released icons])
   		important note: use as an array to add multiple images new Array('path_to_img', 'path_to_img2', ...)
   _img (if the element is supposed to be an image, this is the path to the image)
	must be an array where the first element is the path to the image
	and the second either 'replace' or 'precede' (so the image will replace or precede the text of the _name parameter)
   _class (css class of the element)
   _dclass (css class of the section dropdown)
   _url (should the category button itself be a link, this is the href attribute)
 

Items (Links):
  rnb_declare('link', new Array('_parent', '_name', '_url', '_addimgsrc', '_img',  '_class'));
  Arguments:
   _parent (_id of the parent tag)
   _name (title on the button)
   _url (the hyperlink reference for the button)
   _addimgsrc (path to a small additional image, if one should be added [yes, this is for the small forum/new/released icons])
		important note: use as an array to add multiple images new Array('path_to_img', 'path_to_img2', ...)
   _img (if the element is supposed to be an image, this is the path to the image)
	must be an array where the first element is the path to the image
	and the second either 'replace' or 'precede' (so the image will replace or precede the text of the _name parameter)
   _class (css class of the element)

*/
// - - - - - - - - - - - - - - - - -



/* ----- SETTINGS ---- */
GLOBAL_bar_settings["defaultSectionClass"]          = 'rnb__section';
GLOBAL_bar_settings["defaultSectionContainerClass"] = 'rnb__sectioncontainer';
GLOBAL_bar_settings["defaultButtonClass"]           = 'rnb__button';
GLOBAL_bar_settings["defaultDropdownClass"]         = 'rnb__dropdown';
GLOBAL_bar_settings["dropdownOffsetTop"]            = -5; // temporary workaround, at least until I figure out how to get a height of an object in pixels in js

// CSS position properties (you'll probably want 'absolute' or 'fixed')
GLOBAL_bar_settings["rnbbodyCSSPosition"]           = 'absolute';
GLOBAL_bar_settings["dropdownCSSPosition"]          = 'absolute';

// Adds a custom character with the following Unicode number in front of the links and categories --- '' (emtpy string) means nothing is added
// use this for special characters, so that they are rendered correctly independently of the  document charset
GLOBAL_bar_settings["buttonPrefixCharCode"]         = 187; // 187 is the &raquo; sign

// class name added to the additional images 
GLOBAL_bar_settings["defaultAddImageClass"]         = 'rnb__addimg';


/* ----- SECTIONS ---- */
rnb_declare('section', new Array('revora', 'Revora', '', '', [imageLink+'sections/section-revora.png', 'precede']));
//rnb_declare('section', new Array('spot', 'Spotlight', '', 'rnb__dropdown rnb__spotlight', [imageLink+'sections/section-spotlight.png', 'precede']));
rnb_declare('section', new Array('cnc', 'C&C', '', '', [imageLink+'sections/section-cnc.png', 'precede']));
rnb_declare('section', new Array('bfme', 'BFME', '', '', [imageLink+'sections/section-bfme.png', 'precede']));
rnb_declare('section', new Array('petro', 'Petroglyph', '', '', [imageLink+'sections/section-petro.png', 'precede']));
rnb_declare('section', new Array('etc', 'Other', '', '', [imageLink+'sections/section-etc.png', 'precede']));

//rnb_declare('section', new Array('jobs', 'Help Wanted', '', '', [imageLink+'sections/section-jobs.png', 'precede'], 'https://forums.revora.net/index.php?showforum=456'));
rnb_declare('section', new Array('donate', 'Donate', '', '', [imageLink+'sections/section-donate.png', 'precede'], 'https://www.revora.net/donate'));

//rnb_declare('section', new Array('help', '', '', '', [imageLink+'sections/section-help.png', 'precede'), 'https://www.revora.net/about/technology#networkbar'));


/* ----- DROPDOWNS & ITEMS ---- */

// _____________________________________________________________________________________
// Section Revora

// Main
rnb_declare('link', new Array('revora', 'Front Page', 'https://www.revora.net/'));

// News
rnb_declare('link', new Array('revora', 'News','https://www.revora.net/news'));

// Forums
rnb_declare('link', new Array('revora', 'Forums', 'https://forums.revora.net', imageLink+'extra/forum.gif'));

// IRC
rnb_declare('link', new Array('revora', 'IRC', 'irc://chat.freenode.net:6667/revora'));

// Hosting
rnb_declare('category', new Array('revora', 'revhosting', 'Hosting', '', '', 'rnb__category', '', 'https://www.revora.net/hosting'));
	rnb_declare('link', new Array('revhosting', 'About', 'https://www.revora.net/hosting'));
	rnb_declare('link', new Array('revhosting', 'Packages', 'https://www.revora.net/hosting/packages'));
	rnb_declare('link', new Array('revhosting', 'Get Hosted', 'https://www.revora.net/hosting/get-hosted'));
	rnb_declare('link', new Array('revhosting', 'FAQ', 'https://www.revora.net/hosting/faq'));
	rnb_declare('link', new Array('revhosting', 'Hosting Forum', 'https://forums.revora.net/index.php?showforum=1506', imageLink+'extra/forum.gif'));

// About
rnb_declare('category', new Array('revora', 'revabout', 'About', '', '', 'rnb__category', '', 'https://www.revora.net/about'));
	rnb_declare('link', new Array('revabout', 'General', 'https://www.revora.net/about'));
	rnb_declare('link', new Array('revabout', 'Association', 'https://www.revora.net/about/association'));
	rnb_declare('link', new Array('revabout', 'History', 'https://www.revora.net/about/history'));
	rnb_declare('link', new Array('revabout', 'Staff', 'https://www.revora.net/about/staff'));
	rnb_declare('link', new Array('revabout', 'Technology', 'https://www.revora.net/about/technology'));


/*
// Magazine
rnb_declare('link', new Array('revora', 'Magazine', 'https://www.revora.net/magazine'));
*/


/* OUTDATED & UNMAINTAINED, UNCOMMENT TO USE AGAIN
// _____________________________________________________________________________________
// Section Spotlight
rnb_declare('link', ['spot', 'ModDB Mod of the Year 2010', 'https://forums.revora.net/index.php?showtopic=80430&view=findpost&p=830672', '', 'http://bar.revora.net/images/spotlight/moty2010spot.png', 'rnb__button__spotlight']);
rnb_declare('link', ['spot', 'Commandos 2: Destination Paris 1.40 Released', 'https://forums.revora.net/index.php?showtopic=79893', '', 'http://bar.revora.net/images/spotlight/commandosdp.jpg', 'rnb__button__spotlight']);
*/




// _____________________________________________________________________________________
// Section CNC

// C&C:Online
rnb_declare('category', ['cnc', 'cnconline', 'C&C:Online', '', '', 'rnb__category','','https://cnc-online.net'] );
	rnb_declare('link', ['cnconline', 'Website', 'https://cnc-online.net'] );
	rnb_declare('link', ['cnconline', 'Support', 'https://forums.revora.net/index.php?showforum=2735', imageLink+'extra/forum.gif'] );
	rnb_declare('link', ['cnconline', 'Discussion', 'https://forums.revora.net/index.php?showforum=2731', imageLink+'extra/forum.gif'] );

// Main
rnb_declare('link', new Array('cnc', 'C&C Guild (Command & Conquer Modding)', 'https://www.cncguild.net/'));

// Forums
rnb_declare('category', new Array('cnc', 'cncforums', 'C&C Guild Forums', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=976',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('cncforums', 'News and Discussion', 'https://forums.revora.net/index.php?showforum=1078',imageLink+'extra/forum.gif'));
	rnb_declare('category', new Array('cncforums', 'cncedf', 'Editing Forums', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=976'));
		rnb_declare('link', new Array('cncedf', 'RA2 and TS Editing', 'https://forums.revora.net/index.php?showforum=228',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('cncedf', 'Generals and ZH Editing', 'https://forums.revora.net/index.php?showforum=27',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('cncedf', 'C&C3 Editing', 'https://forums.revora.net/index.php?showforum=1477',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('cncedf', 'RA3 Editing', 'https://forums.revora.net/index.php?showforum=1997',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('cncedf', 'C&C4 Mapping', 'https://forums.revora.net/index.php?showforum=2364',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('cncforums', 'Hosted Mods', 'https://forums.revora.net/index.php?showforum=2144',imageLink+'extra/forum.gif'));	
		
		

// C&C4 Mods (one can dream)
// rnb_declare('category', new Array('cnc', 'cnc4mods', 'C&C4 Modifications', '', '', 'rnb__category', '', ''));
		
// RA3 Mods (re-enable category if and when we get RA3 hostees again)
// rnb_declare('category', new Array('cnc', 'ra3mods', 'RA3 Modifications', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=1862'));

// C&C 3 Mods	
rnb_declare('category', new Array('cnc', 'cnc3mods', 'C&C3 Modifications', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=1413'));
	rnb_declare('link', new Array('cnc3mods', 'Alternate Warfare', 'https://forums.revora.net/forum/2199-alternate-warfare/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('cnc3mods', 'C.A.B.A.L. Rising', 'https://forums.revora.net/index.php?showforum=2569', new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('cnc3mods', 'Command & Conquer 2142', 'http://cnc2142.cncguild.net'));
	rnb_declare('link', new Array('cnc3mods', 'Dark Summer \'96', 'https://forums.revora.net/index.php?showforum=2470', new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('cnc3mods', 'Only War 2', 'http://onlywar2.cncguild.net'));
	rnb_declare('link', new Array('cnc3mods', 'Red Alter', 'https://forums.revora.net/index.php?showforum=2466', new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('cnc3mods', 'Tiberian History', 'https://forums.revora.net/index.php?showforum=2311',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
// Generals Mods	
rnb_declare('category', new Array('cnc', 'genmods', 'Generals Modifications', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=403'));
	rnb_declare('link', new Array('genmods', 'Contra', 'http://contra.cncguild.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('genmods', 'Defcon', 'https://www.cncguild.net/item-255?addview',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('genmods', 'Fallout Wars', 'https://forums.revora.net/index.php?showforum=2497',new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('genmods', 'Only War', 'http://onlywar.cncguild.net',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('genmods', 'Project Raptor', 'http://projectraptor.cncguild.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('genmods', 'Remix Escalation', 'https://forums.revora.net/index.php?showforum=1595', new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
	rnb_declare('link', new Array('genmods', 'Tactical Warfare', 'https://forums.revora.net/index.php?showforum=1414',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('genmods', 'Zero Hour Enhanced', 'https://forums.revora.net/index.php?showforum=2527',new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));

// Red Alert 2 Mods
rnb_declare('category', new Array('cnc', 'ra2mods', 'TS and RA2 Modifications', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=404'));
	rnb_declare('category', new Array('ra2mods', 'ra2mods_a', 'A to I', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=404'));
		rnb_declare('category', new Array('ra2mods_a', 'alliedg', 'Allied General\'s Bunker', '', '', 'rnb__category','','http://agbunker.cncguild.net/'));
			rnb_declare('link', new Array('alliedg', 'AG: Revora Hour', 'https://forums.revora.net/index.php?showforum=807',imageLink+'extra/forum.gif'));
			rnb_declare('link', new Array('alliedg', 'AG: Shattered Alliance', 'https://forums.revora.net/index.php?showforum=1655',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
			rnb_declare('link', new Array('alliedg', 'TS: Evolution', 'https://forums.revora.net/index.php?showforum=830',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('ra2mods_a', 'Brotherhood of Mod', 'http://bhom.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('ra2mods_a', 'C&C Apocalypse', 'http://omegabolt.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('ra2mods_a', 'C&C Dark Rising', 'https://forums.revora.net/index.php?showforum=1786',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('ra2mods_a', 'C&C Reloaded', 'http://reloaded.gamemod.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('ra2mods_a', 'Doom Desire', 'http://ddesire.cncguild.net/'));
		rnb_declare('category', new Array('ra2mods_a', 'enigmas', 'Enigma Productions', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=797'));
			rnb_declare('link', new Array('enigmas', 'Robot Storm', 'http://rstorm.cncguild.net/',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('enigmas', 'YR Warzone', 'https://forums.revora.net/index.php?showforum=796',imageLink+'extra/forum.gif'));
	rnb_declare('category', new Array('ra2mods', 'ra2mods_j', 'J to R', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=404'));
		rnb_declare('link', new Array('ra2mods_j', 'Mental Omega', 'http://mo.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('category', new Array('ra2mods_j', 'migeater', 'Migeater.net', '', '', 'rnb__category','','http://www.migeater.net/'));
			rnb_declare('link', new Array('migeater', 'Code Red', 'http://codered.migeater.net/',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('migeater', 'D-Day', 'http://dday.migeater.net/'));
		rnb_declare('link', new Array('ra2mods_j', 'PreRA2', 'http://prera2.cncguild.net/',imageLink+'extra/new.gif'));
		rnb_declare('link', new Array('ra2mods_j', 'Project Phantom', 'http://phantom.cncguild.net'));
		rnb_declare('link', new Array('ra2mods_j', 'RA2: Total War', 'https://forums.revora.net/index.php?showforum=2330',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('ra2mods_j', 'Revolution: ROTC', 'http://cnc2.cncguild.net/',imageLink+'extra/released.gif'));
	rnb_declare('category', new Array('ra2mods', 'ra2mods_s', 'S to Z', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=404'));
		rnb_declare('link', new Array('ra2mods_s', 'Sudden Strike', 'http://suddenstrike.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('ra2mods_s', 'The Third War', 'http://thirdwar.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('ra2mods_s', 'YR Attacque Sup�rior', 'http://yras.cncguild.net/'));
		rnb_declare('link', new Array('ra2mods_s', 'YR Zero Hour', 'https://forums.revora.net/index.php?showforum=1578',imageLink+'extra/forum.gif'));

// Classic Mods (archived, but of good quality, and released in some form)
rnb_declare('category', new Array('cnc', 'goldmods', 'Community Classics', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=1960'));
		rnb_declare('link', new Array('goldmods', 'Black Missile [RA2]', 'http://blackmissile.cncguild.net',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('goldmods', 'The Brewery [YR]', 'https://forums.revora.net/index.php?showforum=1412'));
		rnb_declare('link', new Array('goldmods', 'Code Red [YR]', 'http://codered.migeater.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('goldmods', 'Command Imperium [ZH]', 'http://imperium.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('goldmods', 'Energy [ZH]', 'http://www.energy.gamemod.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('goldmods', 'Future Crisis [YR]', 'http://fc.cncguild.net/', imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('goldmods', 'It Came From RA! [YR]', 'http://icfra.cncguild.net/',imageLink+'extra/released.gif'));

// Archived Mods	
rnb_declare('category', new Array('cnc', 'archmods', 'Archived Modifications', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=1960'));
	rnb_declare('category', new Array('archmods', 'archra2yr', 'TS / RA2 / YR', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=2196'));
		rnb_declare('category', new Array('archra2yr', 'maryj', 'Bizarre Mind Studios', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=1592'));
				rnb_declare('link', new Array('maryj', 'MJ\'s Rules', 'http://mjrules.cncguild.net/',imageLink+'extra/released.gif'));
				rnb_declare('link', new Array('maryj', 'Terra War', 'http://terrawar.cncguild.net/'));
		rnb_declare('link', new Array('archra2yr', 'C&C Chronostorm', 'http://chronostorm.cncguild.net/'));
		rnb_declare('link', new Array('archra2yr', 'C&C Destiny', 'https://forums.revora.net/index.php?showforum=1319',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archra2yr', 'C&C Nuked', 'https://forums.revora.net/index.php?showforum=1419',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
		rnb_declare('link', new Array('archra2yr', 'C&C Origins', 'https://forums.revora.net/index.php?showforum=1904',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archra2yr', 'CoH: Alerted', 'https://forums.revora.net/index.php?showforum=2200',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archra2yr', 'Generals Mod', 'https://forums.revora.net/index.php?showforum=1778',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archra2yr', 'Imperator', 'https://forums.revora.net/index.php?showforum=1375',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archra2yr', 'RA2: Redux', 'https://forums.revora.net/index.php?showforum=1873',imageLink+'extra/forum.gif'));	
		rnb_declare('link', new Array('archra2yr', 'RA2: Total War', 'https://forums.revora.net/index.php?showforum=2330',imageLink+'extra/forum.gif'))
		rnb_declare('link', new Array('archra2yr', 'Red Renegade', 'https://forums.revora.net/index.php?showforum=1495',imageLink+'extra/forum.gif'));		;
		rnb_declare('link', new Array('archra2yr', 'The Keating Konflux', 'http://konflux.cncguild.net/',imageLink+'extra/released.gif'));
	rnb_declare('category', new Array('archmods', 'archgenzh', 'Generals / ZH', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=2197'));	
		rnb_declare('link', new Array('archgenzh', 'Blitzkrieg 2: TFH', 'http://www.derelictstudios.net/blitz2/index.php',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('archgenzh', 'Conquer or Die', 'http://conquerordie.cncguild.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('archgenzh', 'Epic Generals', 'http://sheelabs.gamemod.net/news_eg.php',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('archgenzh', 'Generals DELTA', 'http://delta.cncguild.net/'));
		rnb_declare('link', new Array('archgenzh', 'Real War', 'https://forums.revora.net/index.php?showforum=790',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archgenzh', 'The Martian War', 'http://martianwar.cncguild.net/'));
		rnb_declare('link', new Array('archgenzh', 'Xeno Force', 'http://xenoforce.gamemod.net/index.htm',imageLink+'extra/released.gif'));
	rnb_declare('category', new Array('archmods', 'archcc3ra3', 'C&C3 / RA3', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=2439'));	
		rnb_declare('link', new Array('archcc3ra3', 'Advanced C&C3 AI Project', 'https://forums.revora.net/index.php?showforum=1704',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archcc3ra3', 'Huhnu Alert 3', 'https://forums.revora.net/index.php?showforum=2361',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('archcc3ra3', 'RA3: Unleashed', 'https://forums.revora.net/index.php?showforum=2315',imageLink+'extra/forum.gif'));
	
// C&C Resources
rnb_declare('category', new Array('cnc', 'cncres', 'Resources', '', '', 'rnb__category','','https://www.cncguild.net'));
	rnb_declare('link', new Array('cncres', 'YR Argentina (C&C: TFD Modding Resources)', 'http://yrarg.cncguild.net/'));
	rnb_declare('link', new Array('cncres', 'Ares Download Mirror (YR Extension DLL)', 'http://ares.cncguild.net/'));
	rnb_declare('link', new Array('cncres', 'GenDev', 'http://gendev.gamemod.net'));
	rnb_declare('category', new Array('cncres', 'ppm', 'Project Perfect Mod', '', '', 'rnb__category','','http://www.ppmsite.com'));
		rnb_declare('category', new Array('ppm', 'ppmmods', 'Modifications', '', '', 'rnb__category','','http://www.ppmsite.com'));
			rnb_declare('link', new Array('ppmmods', 'Dawn of the Tiberium Age', 'http://www.ppmsite.com/forum/index.php?f=504',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'PPM: Final Dawn', 'http://www.ppmsite.com/forum/index.php?f=47'));
			rnb_declare('link', new Array('ppmmods', 'Project: New Tiberia Age', 'http://www.ppmsite.com/forum/index.php?f=80',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'Reign of Steel', 'http://www.ppmsite.com/forum/index.php?f=267'));
			rnb_declare('link', new Array('ppmmods', 'Return of the Dawn', 'http://www.ppmsite.com/forum/index.php?f=116',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'Star Strike', 'http://www.ppmsite.com/forum/index.php?f=245',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'TS: Dusk', 'http://www.ppmsite.com/forum/index.php?f=231',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'TS: Rise of Omnius', 'http://www.ppmsite.com/forum/index.php?f=420'));
			rnb_declare('link', new Array('ppmmods', 'TS: Total War', 'http://www.ppmsite.com/forum/index.php?f=62',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'Tiberian Odyssey', 'http://www.ppmsite.com/forum/index.php?f=178'));
			rnb_declare('link', new Array('ppmmods', 'Tiberian Sun Squared', 'http://www.ppmsite.com/forum/index.php?f=383',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'Tiberium Essence', 'http://www.ppmsite.com/forum/index.php?f=554',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'Tiberium Future: A Dying World', 'http://www.ppmsite.com/forum/index.php?f=155',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmmods', 'Twisted Insurrection', 'http://www.ppmsite.com/forum/index.php?f=466'));
		rnb_declare('category', new Array('ppm', 'ppmutils', 'Utilities', '', '', 'rnb__category','','http://www.ppmsite.com'));
			rnb_declare('link', new Array('ppmutils', 'CnC Editing Tools', 'http://www.ppmsite.com/'));
			rnb_declare('link', new Array('ppmutils', 'OS BIG Editor', 'http://www.ppmsite.com/?go=osbigeditorinfo',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmutils', 'OS SHP Builder', 'http://www.ppmsite.com?go=shpbuilderinfo',imageLink+'extra/released.gif'));
			rnb_declare('link', new Array('ppmutils', 'Voxel Section Editor III', 'http://www.ppmsite.com/?go=vxlseinfo',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('ppm', 'PPM SVN', 'http://svn.ppmsite.com/'));
	rnb_declare('link', new Array('cncres', 'SDI', 'http://sdi.cncguild.net'));
	rnb_declare('link', new Array('cncres', 'Tiberian Graveyards', 'http://graveyards.gamemod.net/style.php?page=downl'));


// _____________________________________________________________________________________
// Section BFME

// Main
rnb_declare('link', new Array('bfme', 'The 3rd Age (BFME Modding)', 'https://www.the3rdage.net/'));

rnb_declare('category', new Array('bfme', 'bfmeonline', 'Battle for Middle-earth Online', '', '', 'rnb__category','','https://t3aonline.net/'));
	rnb_declare('link', new Array('bfmeonline', 'T3A:Online', 'https://t3aonline.net/', imageLink+'extra/new.gif'));
	rnb_declare('link', new Array('bfmeonline', 'T3A:Online Support', 'https://t3aonline.net/support/', imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfmeonline', 'BFME Online Forums', 'https://forums.revora.net/forum/2670-bfme-online/', imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfmeonline', 'Tournament & Games', 'https://forums.revora.net/forum/1585-bfme-tournaments-games/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfmeonline', 'Community Patches', 'http://bfmepatch.the3rdage.net/'));
	rnb_declare('link', new Array('bfmeonline', 'Patch Development', 'https://forums.revora.net/forum/2680-patch-development/',imageLink+'extra/forum.gif'));

// BfME 1 Mods
rnb_declare('category', new Array('bfme', 'bfme1mods', 'Battle for Middle-earth 1 Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/548-the-3rd-age/'));
	rnb_declare('link', new Array('bfme1mods', 'The Dwarf Holds', 'http://tdh.the3rdage.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('bfme1mods', 'Lone Wolf', 'http://lonewolf.the3rdage.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('bfme1mods', 'The 4th Age', 'http://tia.the3rdage.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('bfme1mods', 'Elven Alliance Community Edition', 'http://elven.the3rdage.net',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('bfme1mods', 'Echoes', 'https://forums.revora.net/forum/1008-echoes/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfme1mods', 'Massive Middle Earth', 'https://forums.revora.net/forum/1852-massive-middle-earth/',imageLink+'extra/forum.gif'));

// BfME 2 Mods
rnb_declare('category', new Array('bfme', 'bfme2mods', 'Battle for Middle-earth 2 Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/548-the-3rd-age/'));
	rnb_declare('link', new Array('bfme2mods', 'Special Extended Edition', 'http://see.the3rdage.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('bfme2mods', 'The Peloponnesian Wars', 'https://forums.revora.net/forum/2462-the-peloponnesian-wars/',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('bfme2mods', 'RC Mod', 'http://rcmod.the3rdage.net/',new Array(imageLink+'extra/forum.gif', imageLink+'extra/released.gif')));
	rnb_declare('link', new Array('bfme2mods', 'BfME 1�', 'https://forums.revora.net/forum/2460-bfme-1/',new Array(imageLink+'extra/forum.gif', imageLink+'extra/released.gif')));
	rnb_declare('link', new Array('bfme2mods', 'BattleGrid', 'https://forums.revora.net/forum/2112-battlegrid/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfme2mods', 'Helm\'s Deep Last Hope', 'http://helmsdeeplasthope.the3rdage.net/'));
	
// RofTK Mods
rnb_declare('category', new Array('bfme', 'rotwkmods', 'Rise of the Witch King Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/548-the-3rd-age/'));
	rnb_declare('link', new Array('rotwkmods', 'RJ RotWK', 'http://rjrotwk.the3rdage.net',imageLink+'extra/released.gif'));
	
// Archived Mods
rnb_declare('category', new Array('bfme', 'archivedmods', 'Archived Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/1603-archived-modifications/'));
	rnb_declare('category', new Array('archivedmods', 'bfme1arch', 'BFME1 Archived Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/1603-archived-modifications/'));	
		rnb_declare('link', new Array('bfme1arch', 'BFME+', 'http://bfmeplus.the3rdage.net',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('bfme1arch', 'Expansions', 'https://forums.revora.net/forum/1307-expansions/',imageLink+'extra/forum.gif'));	
		rnb_declare('link', new Array('bfme1arch', 'Grim\'s Hero Mod', 'https://forums.revora.net/forum/730-grim/',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
	
	rnb_declare('category', new Array('archivedmods', 'bfme2arch', 'BFME2 Archived Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/1603-archived-modifications/'));	
		rnb_declare('link', new Array('bfme2arch', 'Battle For The Galaxy', 'http://swbftg.the3rdage.net/'));
		rnb_declare('link', new Array('bfme2arch', 'BFME II Deluxe Edition', 'https://forums.revora.net/forum/1194-bfme-ii-deluxe-edition/',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
		rnb_declare('link', new Array('bfme2arch', 'Fin\'s Modding', 'http://fingulfin.the3rdage.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('bfme2arch', 'Mercenaries Mod', 'https://forums.revora.net/forum/730-grim/',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
		rnb_declare('link', new Array('bfme2arch', 'Rise of Sauron', 'http://riseofsauron.the3rdage.net/'));
		rnb_declare('link', new Array('bfme2arch', 'The Four Ages', 'http://lgda.the3rdage.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('bfme2arch', 'The Last Alliance', 'https://forums.revora.net/forum/1488-the-hobbit/',new Array(imageLink+'extra/forum.gif', imageLink+'extra/released.gif')));
		rnb_declare('link', new Array('bfme2arch', 'Twilight of the Republic', 'http://twilightoftherepublic.the3rdage.net/'));
	
	rnb_declare('category', new Array('archivedmods', 'rotwkarch', 'ROTWK Archived Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/1603-archived-modifications/'));	
		rnb_declare('link', new Array('rotwkarch', 'Age Of Men', 'http://aomexp.the3rdage.net/'));
		rnb_declare('link', new Array('rotwkarch', 'Age of Numenor', 'https://forums.revora.net/forum/2026-age-of-numenor/',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('rotwkarch', 'Age of the Firstborn', 'https://forums.revora.net/forum/1856-age-of-the-firstborn/',new Array(imageLink+'extra/forum.gif', imageLink+'extra/released.gif')));
		rnb_declare('link', new Array('rotwkarch', 'Battles of Gondor', 'http://battlesofgondor.the3rdage.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('rotwkarch', 'G.E.M.', 'http://gem.the3rdage.net/',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('rotwkarch', 'Wars of Arda', 'https://forums.revora.net/forum/2158-the-wars-of-arda/',new Array(imageLink+'extra/forum.gif', imageLink+'extra/released.gif')));
		rnb_declare('link', new Array('rotwkarch', 'Wars of the East', 'http://ithronaiwendil.the3rdage.net',imageLink+'extra/released.gif'));
		
// Completed Mods
rnb_declare('category', new Array('bfme', 'completeddmods', 'Completed Mods', '', '', 'rnb__category','','https://forums.revora.net/forum/2461-completed-modifications/'));
	rnb_declare('link', new Array('completeddmods', 'Arcade-Edition', 'http://arcade-edition.the3rdage.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('completeddmods', 'Kings of the West', 'http://gothmogtheorc.the3rdage.net/',imageLink+'extra/released.gif'));
	
// Resources
rnb_declare('category', new Array('bfme', 'bfme_res', 'Resources', '', '', 'rnb__category','','https://www.the3rdage.net/'));
	rnb_declare('link', new Array('bfme_res', 'BFME Forums', 'https://forums.revora.net/forum/548-the-3rd-age/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfme_res', 'T3A News', 'https://forums.revora.net/forum/568-t3a-news/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfme_res', 'LOTR Videogames', 'https://forums.revora.net/forum/2359-lord-of-the-rings-videogames/',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('bfme_res', 'LOTR Lore', 'https://forums.revora.net/forum/2360-lord-of-the-rings-lore/',imageLink+'extra/forum.gif'));


// _____________________________________________________________________________________
// Section Petroglyph

// Main
rnb_declare('link', new Array('petro', 'Petrolution (Petroglyph Modding)', 'http://www.petrolution.net/'));
rnb_declare('link', new Array('petro', 'Petrolution Forums', 'https://forums.revora.net/index.php?showforum=799'));
rnb_declare('link', new Array('petro', 'Petrolution Mod Tools', 'http://modtools.petrolution.net/'));

// Empire At War Mods
rnb_declare('category', new Array('petro', 'eawmods', 'Empire At War Mods', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('eawmods', 'Eras of the Mercenaries', 'https://forums.revora.net/index.php?showforum=2259',new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('eawmods', 'Gundam Legacy', 'http://gundamlegacy.petrolution.net/',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('eawmods', 'Imperia: Rise of the Time Empire', 'https://forums.revora.net/index.php?showforum=2265',new Array(imageLink+'extra/forum.gif',imageLink+'extra/new.gif')));
	rnb_declare('link', new Array('eawmods', 'Phoenix Rising', 'http://www.eawpr.net',imageLink+'extra/released.gif'));
	rnb_declare('link', new Array('eawmods', 'Sci-Fi at War', 'https://forums.revora.net/index.php?showforum=2208',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('eawmods', 'Warhammer 40,000 Universal War', 'https://forums.revora.net/index.php?showforum=2174',imageLink+'extra/forum.gif'));
// Universe At War Mods
rnb_declare('category', new Array('petro', 'uawmods', 'Universe At War Mods', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('uawmods', 'XenoProject', 'https://forums.revora.net/index.php?showforum=2081',imageLink+'extra/forum.gif'));


// _____________________________________________________________________________________
// Section Other

// Dawn of War Projects
rnb_declare('category', new Array('etc', 'dow', 'Dawn of War', '', '', 'rnb__category','','https://forums.revora.net/index.php?showforum=478'));
	rnb_declare('link', new Array('dow', 'Dawn of War Forums', 'https://forums.revora.net/index.php?showforum=478',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'Dawn Of Skirmish DC', 'https://forums.revora.net/index.php?showforum=478',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
	rnb_declare('link', new Array('dow', 'Corsix DoW Mod Studio', 'https://forums.revora.net/index.php?showforum=951',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
	rnb_declare('link', new Array('dow', 'Dark Angels', 'https://forums.revora.net/index.php?showforum=1911',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'Eldar Craftworlds', 'https://forums.revora.net/index.php?showforum=2115',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'Harlequins', 'https://forums.revora.net/index.php?showforum=1962',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'HisRighteous', 'https://forums.revora.net/index.php?showforum=1986',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'ThounsandSons', 'https://forums.revora.net/index.php?showforum=2005',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'Ultra Marines', 'https://forums.revora.net/index.php?showforum=1925',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('dow', 'Wolf Lords', 'https://forums.revora.net/index.php?showforum=2061',imageLink+'extra/forum.gif'));

// Commandos HQ
rnb_declare('category', new Array('etc', 'commandos', 'Commandos', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('commandos', 'Commandos HQ', 'http://commandoshq.net/'));
	rnb_declare('link', new Array('commandos', 'Commandos HQ Forums', 'https://forums.revora.net/index.php?showforum=2302',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('commandos', 'Commandos 2: Destination Paris', 'http://commandoshq.net/c2dp_info.php'));

// Game Development Projects
rnb_declare('category', ['etc', 'gamedev', 'Game Development', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar']);
	rnb_declare('link', ['gamedev', 'Star Villains and Space Heroes', 'http://svsh-game.com/', imageLink+'extra/new.gif']);
	rnb_declare('link', ['gamedev', 'Capalex65\'s BYOND Gaming', 'http://byond.gamemod.net/']);
	rnb_declare('link', ['gamedev', 'Destructivality', 'http://www.destructivality.com/']);
	rnb_declare('link', ['gamedev', 'It Came From The Cave','https://forums.revora.net/index.php?showforum=2023',[imageLink+'extra/forum.gif',imageLink+'extra/released.gif']]);
	rnb_declare('link', ['gamedev', 'Shee Labs', 'http://sheelabs.gamemod.net/']);

// Half Life Projects
rnb_declare('category', new Array('etc', 'hls', 'Half Life Series', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('category', new Array('hls', 'hl1mods', 'Half Life Mods', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
		rnb_declare('link', new Array('hl1mods', 'Zion Warcry', 'https://forums.revora.net/index.php?showforum=621',new Array(imageLink+'extra/forum.gif',imageLink+'extra/released.gif')));
	rnb_declare('category', new Array('hls', 'hl2mods', 'Half Life 2 Mods', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
		rnb_declare('link', new Array('hl2mods', 'Art Of Ascension', 'http://aoa.gamemod.net',imageLink+'extra/released.gif'));
		rnb_declare('link', new Array('hl2mods', 'Project Citizen', 'https://forums.revora.net/index.php?showforum=1703',imageLink+'extra/forum.gif'));
		rnb_declare('link', new Array('hl2mods', 'Squizz Studios', 'http://squizzstudios.gamemod.net/'));

// Portfolios
rnb_declare('category', new Array('etc', 'portfolio', 'Portfolios', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('portfolio', 'spyvspy', 'http://spyVspy.revora.net/'));

// Comics
rnb_declare('category', new Array('etc', 'comics', 'Comics', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('comics', 'Patrol Comics', 'http://patrol.revora.net/'));

// Sounds & Music
rnb_declare('category', new Array('etc', 'sounds', 'Sounds or Music', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('sounds', 'Sound FX Vault', 'https://forums.revora.net/index.php?showforum=1685',imageLink+'extra/forum.gif'));

// RPGs
rnb_declare('category', new Array('etc', 'rpgs', 'RPGs', '', '', 'rnb__category','','https://www.revora.net/about/technology#networkbar'));
	rnb_declare('link', new Array('rpgs', 'Arsencia', 'https://forums.revora.net/index.php?showforum=1744',imageLink+'extra/forum.gif'));
	rnb_declare('link', new Array('rpgs', 'Millennium 3000', 'https://forums.revora.net/index.php?showforum=2368',imageLink+'extra/forum.gif'));

// Stories WHO NEEDS STORIES

// Other Divisions
rnb_declare('link', new Array('etc', 'Role-Playing Games Forums', 'https://forums.revora.net/index.php?showforum=1421'));



// _____________________________________________________________________________________
// Section Help
//rnb_declare('link', new Array('help', 'About the Network Bar', 'https://www.revora.net/about/technology#networkbar'));

// Initialise the bar
rnb_construct();

// #EoF# //
