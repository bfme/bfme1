/* 
***********************************
The Revora Network Bar - Bar Include
***********************************

File: bar.js
Author: 
  Original: Blodo [Yuri_S] 
  Current version: Dark Lord of the Sith (DLotS)
  Description: This file includes the other required files
*/
// - - - - - - - - - - - -


// this allows local testing
if(typeof(rnb_path) == 'undefined' || rnb_path == null)
{
	var rnb_path = '//bar.revora.net/';
}


// include functions:
if (typeof document.body.style.maxHeight != "undefined")
{
	var body = document.body || document.getElementsByTagName('body')[0];
	var head = document.head || document.getElementsByTagName('head')[0];
	
	// include bar system
	var sys = document.createElement('script');
	sys.language = 'javascript';
	sys.type = 'text/javascript';
	sys.src = rnb_path+'bar_system.js';
	sys.async = false;
	body.appendChild(sys);

	// if no styleLink has been set, use the default style
	if(typeof(styleLink) == 'undefined' || styleLink == null)
	{
		styleLink = rnb_path+'rnb_default.css';
	}
	// include css:css
	var css = document.createElement('link');
	css.href = styleLink;
	css.type = 'text/css';
	css.rel = 'stylesheet';
	head.appendChild(css);


	// if no imageLink has been set, use the default set of images
	if(typeof(imageLink) == 'undefined' ||  imageLink == null)
	{
		imageLink = rnb_path+'images/';
	}

	// include directives:
	var cont = document.createElement('script');
	cont.language = 'javascript';
	cont.type = 'text/javascript';
	cont.src = rnb_path+'bar_content.js';
	cont.async = false;
	body.appendChild(cont);
}

// #EoF# //